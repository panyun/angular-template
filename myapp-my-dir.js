/**
 * 名称：
 * 版本：
 * 作用：
 * 使用方法：
 * 作者：
 * 首次提交日期：
 * 最近提交日期：
 */
var module = angular.module('myapp.my-dir', []);

module.directive('myappMyDir', ['$compile', function($compile) {

    function compile(tElement, tAttrs, transclude) {
        //compile函数在link函数之前执行。
        //compile函数的返回结果是link函数。因此，compile和link函数不能同时使用。
    }

    /**
     * scope: 当前指令的scope。默认情况下与parent的scope相同。
     * iElement: 应用当前指令的HTML元素。HTML元素已被jQlite包装。
     * iAttrs: 是一个包含了属性名和值的map。属性名和值在由指令表示的HTML元素中添加。
     */
    function link(scope, iElement, iAttrs, controller, transcludeFn) {
        //设置scope中的model供template使用
        //监听model的变化
        //为DOM元素添加事件监听器
        //其他所有DOM操作...
    }

    var directiveDefinitionObject = {
        controller: 'myappMyDirCtrl',
        link: link,
        restrict: 'AEC',
        replace: true,
        scope: {
            usage: "=",
            itemsByPage: '@'
        },
        templateUrl: 'myapp-my-dir.tpl.html'
    };

    return directiveDefinitionObject;
}]);

module.controller('myappMyDirCtrl', ['$scope', '$http',
    function($scope, $http) {

    }
]);
