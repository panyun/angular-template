var app = angular.module('myapp.example-service', []);

app.factory('ExampleService', ['$http', 'OtherService', function($http, OtherService) {

    var self = this; //解决不同context下this含义差异带来的区别
    var myData = {}; //需要共享的数据

    //功能函数
    var getMyData = function() {
        return self.myData;
    };

    //功能函数
    var myFunction1 = function() {
        var uri = '/myuri';
        //此处编写业务代码...
        return $http({
            method: "GET",
            url: uri,
            cache: false
        });
    };

    //功能函数
    var myFunction2 = function(parameters) {
        var uri = '/myuri';
        parameters = parameters || {};
        //此处编写业务代码...
        return $http({
            method: "GET",
            url: uri,
            params: parameters,
            cache: false
        });
    };

    //功能函数
    var myFunction3 = function() {
        var uri = '/myuri';
        var myPostData = {};
        //此处编写业务代码...
        return $http({
            method: "POST",
            url: uri,
            data: myPostData,
            cache: false
        });
    };

    //封装了功能函数的JSON对象
    var serviceInstance = {
        getMyData: getMyData,
        myFunction1: myFunction1,
        myFunction2: myFunction2,
        myFunction3: myFunction3
    };

    return serviceInstance;
}]);
